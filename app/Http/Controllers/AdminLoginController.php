<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    //Admin Login
    public function login(){
        return view('admin.auth.login');

    }
}
