<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Admin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
            'admin_name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
             'created_at' => now(),
             'updated_at' => now(),

        ]);
    
    }
}
